function sendAjaxRequest(method, url, body, requestHeaders, successCallback, failureCallback){
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);

    for(let requestHeaderKey in requestHeaders){
        // {"Authorization" : "auth-token", "Content-Type": "application/json"}
        xhr.setRequestHeader(requestHeaderKey, requestHeaders[requestHeaderKey]);
    }
    xhr.onreadystatechange = function () {
      if (xhr.readyState === 4) {
          console.log(xhr);
        if (xhr.status>199 && xhr.status<300) {
          successCallback(xhr);
        } else {
          failureCallback(xhr);
        }
      }
    };
    if(body){
        xhr.send(body);
    } else {
        xhr.send();
    }
}

function sendAjaxGet(url, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("GET", url, null, requestHeaders, successCallback, failureCallback);
}

function ajaxGetTracks(authToken, successCallback, failureCallback){
    let requestUrl = "http://localhost:8082/tracks";
    let requestHeaders = {"Authorization": authToken};
    sendAjaxGet(requestUrl, requestHeaders, successCallback, failureCallback);
}

function sendAjaxPost(url, body, requestHeaders, successCallback, failureCallback){
    sendAjaxRequest("POST",url, body, requestHeaders, successCallback, failureCallback);
}

function ajaxCreateTrack(body, authToken, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/json", "Authorization":authToken};
    let requestUrl = "http://localhost:8082/tracks";
    sendAjaxPost(requestUrl, body, requestHeaders, successCallback, failureCallback);
}

function ajaxLogin(username, password, successCallback, failureCallback){
    let requestHeaders = {"Content-Type":"application/x-www-form-urlencoded"};
    let requestUrl = "http://localhost:8082/login";
    let requestBody = `username=${username}&password=${password}`;
    sendAjaxPost(requestUrl, requestBody, requestHeaders, successCallback, failureCallback);
}