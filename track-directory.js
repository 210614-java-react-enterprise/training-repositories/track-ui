// create a HTTP request when the page loads

window.onload = function () {

  const token = sessionStorage.getItem("token");
  if(token){
    ajaxGetTracks(token, renderTracks, logError);
  } else {
    window.location.href = "./login.html";
  }
  
};

function renderTracks(xhr) {
  const tracksJson = xhr.responseText;
  const tracks = JSON.parse(tracksJson);
  const trackHtml = tracks
    .map(
      (track) =>
        `<li id="track-list-item-${track.id}" class="list-group-item"> ${track.name}  - ${track.artist} </li>`
    )
    .join("");
  document.getElementById("track-list").innerHTML = trackHtml;
}

function logError(){
  console.log("there was an issue with the request");
}
