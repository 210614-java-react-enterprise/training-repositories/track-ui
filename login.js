document.getElementById("login-form").addEventListener("submit", attemptLogin);

function attemptLogin(event){
    event.preventDefault();
    
    const username = document.getElementById("inputUsername").value;
    const password = document.getElementById("inputPassword").value;

    ajaxLogin(username,password, loginSuccess, loginFail);
}

function loginSuccess(xhr){
    let token = xhr.getResponseHeader("Authorization");
    sessionStorage.setItem("token", token);
    window.location.href = "./home.html";
}

function loginFail(){
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;

}