window.onload = function(){
  document
  .getElementById("new-track-form")
  .addEventListener("submit", submitNewTrack);

}


function submitNewTrack(event) {
  event.preventDefault(); // prevent form submission from making a request
  // send an HTTP (xjax), this request will be a POST request, needs a request body

  // get values from the input fields on our webpage
  const trackName = document.getElementById("track-name").value;
  const trackArtist = document.getElementById("track-artist").value;
  const releaseYear = document.getElementById("track-year").value;

  // create a track object
  const track = { name: trackName, artist: trackArtist, releaseYear };

  // stringify track object
  const trackJson = JSON.stringify(track);

  // provide JSON representation of track as an argument to the send method

  const token = sessionStorage.getItem("token");
  if (token) {
    ajaxCreateTrack(trackJson, token, indicateSuccess, indicateFailure)
  } else {
    //redirect elsewhere
    // console.log("no token provided");
    window.location.href = "./login.html";
  }
}

function indicateSuccess() {
  indicateResult("Successfully added new track!");
}

function indicateFailure() {
  indicateResult("There was an issue creating your new record");
}

function indicateResult(message) {
  const messageDiv = document.getElementById("create-msg");
  messageDiv.hidden = false;
  messageDiv.innerText = message;
}
